# 使用官方 Python 基礎映像
FROM python:slim

# 設置工作目錄
WORKDIR /app

# 複製需求文件並安裝 Python 依賴
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# 複製應用程式代碼
COPY . .

# 暴露應用程式端口
EXPOSE 8000

# 定義運行應用程式的命令
CMD ["python", "app/server.py"]
