import os
import json
import uvicorn
from fastapi import FastAPI, Request, HTTPException
from fastapi.responses import JSONResponse, HTMLResponse
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from google.cloud import bigquery
from google.oauth2 import service_account

# list for relative major
relevant_majors = [
    "電機／電子工程學系",
    "資訊管理學系",
    "資訊工程學系",
    "機械工程學系（控制組、機器人）",
    "資料科學系（大數據、人工智慧）",
]

app = FastAPI()

credentials_path = "./testbigquery-423415-f1f3526bb24a.json"

if not credentials_path or not os.path.exists(credentials_path):
    raise FileNotFoundError(
        "服務帳戶憑證文件未找到，請確認 GOOGLE_APPLICATION_CREDENTIALS 設置正確。"
    )


with open("./mock.json", "r", encoding="utf-8") as file:
    mock_data = json.load(file)


# 定義資料模型
class DataModel(BaseModel):
    id: int
    科系: str
    職稱: str
    年薪: int
    年資: int
    產業: str


# 查詢 BigQuery 的函數
def query_bigquery(project_id, query):
    try:
        credentials = service_account.Credentials.from_service_account_file(
            credentials_path,
            scopes=[
                "https://www.googleapis.com/auth/cloud-platform",
                "https://www.googleapis.com/auth/drive",
                "https://www.googleapis.com/auth/bigquery",
            ],
        )
        client = bigquery.Client(credentials=credentials, project=project_id)
    except Exception as e:
        print(f"創建BigQuery客戶端時發生錯誤: {e}")
        return None

    try:
        query_job = client.query(query)
        results = query_job.result()
        return results
    except Exception as e:
        print(f"執行查詢時發生錯誤: {e}")
        return None


# CORS 設置
app.add_middleware(
    CORSMiddleware,
    # allow_origins=[
    #     "https://techporn-dashboard-testgroup1901949-9d6e5ed9bff9762f1ce9dfdd4ec.gitlab.io"
    # ],
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/", response_class=HTMLResponse)
async def hello_world(request: Request):
    print(request)
    return "<p>Hello, World!!</p>"


# 獲取數據
@app.get("/api/data")
async def get_data(email: str = ""):
    if len(email) == 0:
        error_response = {
            "error": "Bad Request",
            "message": "Please provide user email",
        }
        raise HTTPException(status_code=400, detail=error_response)

    project_id = "testbigquery-423415"
    query = f"""
    WITH t1 AS (
        SELECT 
                ROW_NUMBER() OVER() AS row_num,
                string_field_6 AS job_title,
                string_field_99 AS annual_salary,
                string_field_7 AS seniority,
                string_field_88 AS industry,
                string_field_5 AS major,
                string_field_93 AS manager,
                string_field_89 AS foreign_businessman,
                string_field_4 AS education_background,
                string_field_90 AS working_hours,
                string_field_86 AS company_location,
                string_field_85 AS company_name ,
                string_field_120 AS company_advantages,
                string_field_121 AS company_shortcomings
            FROM `testbigquery-423415.techporn_formdata.techporn_formdata`
            WHERE 
                string_field_0 != "時間戳記"
        ), t2 AS (
            SELECT 
                ROW_NUMBER() OVER() AS row_num,
                CONCAT(
                    IFNULL(string_field_10, ''), IFNULL(string_field_16, ''), IFNULL(string_field_22, ''), 
                    IFNULL(string_field_28, ''), IFNULL(string_field_33, ''), IFNULL(string_field_39, ''), 
                    IFNULL(string_field_43, ''), IFNULL(string_field_49, ''), IFNULL(string_field_55, ''), 
                    IFNULL(string_field_61, ''), IFNULL(string_field_67, ''), IFNULL(string_field_71, ''), 
                    IFNULL(string_field_75, ''), IFNULL(string_field_79, '')) AS most_used_skill,
                CONCAT(
                    IFNULL(string_field_11, ''), IFNULL(string_field_17, ''), IFNULL(string_field_23, ''), 
                    IFNULL(string_field_29, ''), IFNULL(string_field_34, ''), IFNULL(string_field_40, ''), 
                    IFNULL(string_field_44, ''), IFNULL(string_field_50, ''), IFNULL(string_field_56, ''), 
                    IFNULL(string_field_62, ''), IFNULL(string_field_68, ''), IFNULL(string_field_72, ''), 
                    IFNULL(string_field_76, ''), IFNULL(string_field_80, '')
                ) AS skill,
                CONCAT(
                    IFNULL(string_field_12, ''), IFNULL(string_field_18, ''), IFNULL(string_field_24, ''), 
                    IFNULL(string_field_35, ''), IFNULL(string_field_45, ''), IFNULL(string_field_51, ''), 
                    IFNULL(string_field_57, ''), IFNULL(string_field_63, ''), IFNULL(string_field_81, '')
                ) AS Framework_Library,
                CONCAT(
                    IFNULL(string_field_13, ''), IFNULL(string_field_19, ''), IFNULL(string_field_25, ''), 
                    IFNULL(string_field_30, ''), IFNULL(string_field_36, ''), IFNULL(string_field_46, ''), 
                    IFNULL(string_field_52, ''), IFNULL(string_field_58, ''), IFNULL(string_field_64, ''), 
                    IFNULL(string_field_82, '')
                ) AS database
            FROM `testbigquery-423415.techporn_formdata.techporn_formdata`
            WHERE 
                string_field_0 != "時間戳記"
        ), t3 AS (
            SELECT 
                ROW_NUMBER() OVER() AS row_num,
                string_field_105 AS open_and_innovative,
                string_field_106 AS competition_among_peers_is_fierce,
                string_field_107 AS difficulty_of_promotion_in_the_company,
                string_field_108 AS happy_atmosphere_among_colleagues,
                string_field_109 AS flexible_company_system,
                string_field_110 AS the_company_operations_are_highly_stable,
                string_field_111 AS work_at_a_fast_pace,
                string_field_112 AS high_error_tolerance,
                string_field_113 AS the_company_has_an_excellent_mentor_system,
                string_field_114 AS normal_working_hours,
                string_field_115 AS flexibility_of_leave_system,
                string_field_116 AS reasonable_distribution_of_work_load,
                string_field_117 AS salary_matches_work_intensity_well,
                string_field_118 AS high_skill_growth_rate,
                string_field_119 AS highly_gender_friendly,

            FROM `testbigquery-423415.techporn_formdata.techporn_formdata`
            WHERE 
                string_field_0 != "時間戳記"
        ), email_check AS (
            SELECT 
                CASE 
                    WHEN COUNT(*) > 0 THEN 1
                    ELSE 0
                END AS email_exists
            FROM `testbigquery-423415.techporn_formdata.techporn_formdata`
            WHERE string_field_1 = '{email}'
        )
        SELECT 
            t1.job_title,
            t1.annual_salary,
            t1.seniority,
            t1.industry,
            t1.major,
            t1.manager,
            t1.foreign_businessman,
            t1.education_background,
            t1.working_hours,
            t1.company_location,
            t1.company_name,
            t1.company_advantages,
            t1.company_shortcomings,
            t2.most_used_skill,
            t2.skill,
            t2.framework_library,
            t2.database,
            t3.open_and_innovative,
            t3.competition_among_peers_is_fierce,
            t3.difficulty_of_promotion_in_the_company,
            t3.happy_atmosphere_among_colleagues,
            t3.flexible_company_system,
            t3.the_company_operations_are_highly_stable,
            t3.work_at_a_fast_pace,
            t3.high_error_tolerance,
            t3.the_company_has_an_excellent_mentor_system,
            t3.normal_working_hours,
            t3.flexibility_of_leave_system,
            t3.reasonable_distribution_of_work_load,
            t3.salary_matches_work_intensity_well,
            t3.high_skill_growth_rate,
            t3.highly_gender_friendly,
            ec.email_exists,
        FROM t1
        JOIN t2 ON t1.row_num = t2.row_num
        JOIN t3 ON t1.row_num = t3.row_num
        CROSS JOIN email_check ec
        """

    results = query_bigquery(project_id, query)
    if results:
        output = []
        id_counter = 1
        email_exists = None
        for row in results:
            email_exists = row.email_exists == 1
            output.append(
                {
                    "id": id_counter,
                    "job_title": row.job_title,
                    "annual_salary": row.annual_salary,
                    "seniority": row.seniority,
                    "industry": row.industry,
                    "skill": {
                        "most_used_program": row.most_used_skill,
                        "program": row.skill.split(", "),
                        "frameworklibrary": row.framework_library.split(", "),
                        "database": row.database.split(", "),
                    },
                    "relevant_majors": row.major in relevant_majors,
                    "major": row.major,
                    "manager": row.manager == "是",
                    "foreign_businessman": row.foreign_businessman == "是",
                    "education_background": row.education_background,
                    "working_hours": row.working_hours,
                    "company_location": row.company_location,
                    "company_name": row.company_name,
                    "company_advantages": row.company_advantages,
                    "company_shortcomings": row.company_shortcomings,
                    "company_atmosphere": {
                        "open_and_innovative": (
                            int(row.open_and_innovative)
                            if row.open_and_innovative
                            else None
                        ),
                        "competition_among_peers_is_fierce": (
                            int(row.competition_among_peers_is_fierce)
                            if row.competition_among_peers_is_fierce
                            else None
                        ),
                        "difficulty_of_promotion_in_the_company": (
                            int(row.difficulty_of_promotion_in_the_company)
                            if row.difficulty_of_promotion_in_the_company
                            else None
                        ),
                        "happy_atmosphere_among_colleagues": (
                            int(row.happy_atmosphere_among_colleagues)
                            if row.happy_atmosphere_among_colleagues
                            else None
                        ),
                        "flexible_company_system": (
                            int(row.flexible_company_system)
                            if row.flexible_company_system
                            else None
                        ),
                        "the_company_operations_are_highly_stable": (
                            int(row.the_company_operations_are_highly_stable)
                            if row.the_company_operations_are_highly_stable
                            else None
                        ),
                        "work_at_a_fast_pace": (
                            int(row.work_at_a_fast_pace)
                            if row.work_at_a_fast_pace
                            else None
                        ),
                        "high_error_tolerance": (
                            int(row.high_error_tolerance)
                            if row.high_error_tolerance
                            else None
                        ),
                        "the_company_has_an_excellent_mentor_system": (
                            int(row.the_company_has_an_excellent_mentor_system)
                            if row.the_company_has_an_excellent_mentor_system
                            else None
                        ),
                        "normal_working_hours": (
                            int(row.normal_working_hours)
                            if row.normal_working_hours
                            else None
                        ),
                        "flexibility_of_leave_system": (
                            int(row.flexibility_of_leave_system)
                            if row.flexibility_of_leave_system
                            else None
                        ),
                        "reasonable_distribution_of_work_load": (
                            int(row.reasonable_distribution_of_work_load)
                            if row.reasonable_distribution_of_work_load
                            else None
                        ),
                        "salary_matches_work_intensity_well": (
                            int(row.salary_matches_work_intensity_well)
                            if row.salary_matches_work_intensity_well
                            else None
                        ),
                        "high_skill_growth_rate": (
                            int(row.high_skill_growth_rate)
                            if row.high_skill_growth_rate
                            else None
                        ),
                        "highly_gender_friendly": (
                            int(row.highly_gender_friendly)
                            if row.highly_gender_friendly
                            else None
                        ),
                    },
                }
            )
            id_counter += 1

        output.pop()  # remove last invalid data
        output.extend(mock_data["data"])  # concat mock data after real data
        return JSONResponse(
            {"data": output, "email_exists": email_exists},
            status_code=200,
        )
    else:
        return {"data": []}


# 404 處理器
@app.exception_handler(404)
async def not_found(request: Request, exc: HTTPException):
    return JSONResponse(status_code=404, content="")


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0")
