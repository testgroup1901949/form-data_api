"id": id,
"職稱": job_title,
"年薪": annual_salary,
"年資": seniority,
"產業": industry,
"最常使用的程式語言" : most_used_skill
"技能": skill,
{
"程式語言": program,
"框架 Library": frameworklibrary,
"資料庫": database,
}
"本科": relevant_majors,
"科系": major,
"主管": manager ,
"是否為外商": foreign_businessman,
"教育背景": education_background,
"工時": working_hours,
"地區": company_location,
"公司名稱": company_name,
"公司的優點": company_advantages,
"公司的缺點": company_shortcomings,

"公司氣氛": company_atmosphere
{
"開放且創新": open_and_innovative,
"同儕競爭激烈": competition_among_peers_is_fierce,
"公司升遷難度": difficulty_of_promotion_in_the_company,
"同事氛圍歡樂": happy_atmosphere_among_colleagues,
"公司制度靈活": flexible_company_system,
"公司營運穩定度高": the_company_operations_are_highly_stable,
"工作節奏快速": work_at_a_fast_pace,
"錯誤包容性高": high_error_tolerance ,
"公司有優良的 Mentor 制度": the_company_has_an_excellent_mentor_system,
"工時正常": normal_working_hours,
"休假制度彈性": flexibility_of_leave_system,
"工作負擔分配合理": reasonable_distribution_of_work_load,
"薪資與工作強度的匹配程度高": salary_matches_work_intensity_well,
"技能成長幅度高": high_skill_growth_rate,
"性別友善程度高": highly_gender_friendly,
}
